module.exports = (n, fn) => {
  const $ = (previousArgs = []) => (...currentArgs) => {
    const commonArgs = [...previousArgs, ...currentArgs];
    return n <= commonArgs.length ? fn(...commonArgs) : $(commonArgs);
  };
  return $();
};
