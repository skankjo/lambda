const curry = require('./curry');
const isNil = require('./isNil');

module.exports = curry((defaultVal, val) => (isNil(val) || isNaN(val) ? defaultVal : val));
