module.exports = (iterator, acc, list) => list.reduce(iterator, acc);
