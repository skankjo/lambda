const pipe = require('./pipe');
const not = require('./not');

module.exports = fn =>
  pipe(
    fn,
    not
  );
