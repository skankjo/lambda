const tail = require('./tail');
const head = require('./head');
const reduce = require('./reduce');

module.exports = (...args) => (...inputs) => reduce((val, fn) => fn(val), head(args)(...inputs), tail(args));
