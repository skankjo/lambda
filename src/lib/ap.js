const reduce = require('./reduce');
const map = require('./map');

module.exports = (apply, val) => {
  if (apply.ap) {
    return apply.ap(val);
  }
  return reduce((acc, f) => acc.concat(map(f, val)), new apply.__proto__.constructor(), apply);
};
