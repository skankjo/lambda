const curry = require('./curry');

module.exports = curry((a, b) => a.concat(b));
