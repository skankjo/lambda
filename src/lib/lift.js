const reduce = require('./reduce');
const head = require('./head');
const tail = require('./tail');
const ap = require('./ap');
const map = require('./map');

module.exports = fn => (...args) => reduce(ap, map(fn, head(args)), tail(args));
