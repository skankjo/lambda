const curry = require('./curry');

module.exports = curry((sep, arr) => arr.join(sep));
