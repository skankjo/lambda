const curry = require('./curry');

module.exports = curry((props, obj) => props.map(prop => obj[prop]));
