const curryN = require('./curryN');

module.exports = fn => curryN(fn.length, fn);
