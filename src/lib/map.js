const curry = require('./curry');
const curryN = require('./curryN');

module.exports = curry((fn, functor) => {
  if (functor.map) {
    return functor.map(arg => fn(arg));
  }

  if (functor instanceof Function) {
    return curryN(fn.length, (...args) => functor(fn(...args)));
  }

  return fn(functor);
});
