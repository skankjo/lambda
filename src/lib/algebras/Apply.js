const Functor = require('./Functor');

module.exports = class Apply extends Functor {
  /**
   *
   * @param {Function} fn
   * @return {object}
   */
  static of(fn) {
    return new Apply(fn);
  }
  /**
   *
   * @constructor
   * @param {function} fn
   */
  constructor(fn) {
    super(fn);
  }

  /**
   *
   * @param {Apply} val
   * @return {Apply}
   */
  ap(val) {
    return val.map(this.val);
  }
};
