const util = require('util');

module.exports = class Functor {
  /**
   *
   * @param {any} val
   * @return {object}
   */
  static of(val) {
    return new Functor(val);
  }
  /**
   * @constructor
   * @param {any} val
   */
  constructor(val) {
    this.val = val;
  }

  /**
   *
   * @param {function} fn
   * @return {any} a result of applying a function
   */
  map(fn) {
    return new this.constructor(fn(this.val));
  }

  /**
   * @return {string}
   */
  toString() {
    return `${this.val}`;
  }

  /**
   * @return {string}
   */
  [util.inspect.custom]() {
    return this.toString();
  }
};
