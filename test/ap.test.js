const test = require('ava');
const { ap, Apply, curry } = require('../src');

test('Should apply Applicative Functor to the value', t => {
  const result = ap(Apply.of(a => a + 1), [1, 2, 3]);

  t.deepEqual([2, 3, 4], result);
});

test('Should apply a list of functions to an array of values', t => {
  const add = curry((a, b) => a + b);
  const functions = [a => a, add(1)];
  const values = [1, 2, 3];

  const result = ap(functions, values);

  t.deepEqual([1, 2, 3, 2, 3, 4], result);
});

test('Should apply a list of curried functions to an array of values', t => {
  const add = curry((a, b) => a + b);

  const additions = [add(1), add(2), add(3)];
  const values = [1, 2, 3];

  const result = ap(additions, values);

  t.deepEqual([2, 3, 4, 3, 4, 5, 4, 5, 6], result);
});

test('Should apply a list of curried functions to a value', t => {
  const add = curry((a, b) => a + b);

  const additions = [add(1)];

  const result = ap(additions, 1);

  t.deepEqual([2], result);
});
