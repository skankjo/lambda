const test = require('ava');
const { join } = require('../src');

test('Should join two strings', t => {
  t.is('ab', join('', ['a', 'b']));
});

test('Should join two strings with a selected separator', t => {
  t.is('a|b', join('|', ['a', 'b']));
});

test('Should join multiple string with selected separator', t => {
  t.is('a b c d', join(' ', ['a', 'b', 'c', 'd']));
});

test('Should join strings curried', t => {
  t.is('a b', join(' ')(['a', 'b']));
});
