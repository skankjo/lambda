const test = require('ava');
const { defaultTo } = require('../src');

test('Should return a value', t => {
  t.is(1, defaultTo(999, 1));
});

test('Should return a default for undefined', t => {
  t.is('default', defaultTo('default', void 0));
});

test('Should return default for null value', t => {
  t.is('default', defaultTo('default', null));
});

test('Should return an empty string', t => {
  t.is('', defaultTo('default', ''));
});

test('Should return a zero', t => {
  t.is(0, defaultTo(999, 0));
});

test('Should return default value for NaN', t => {
  t.is(999, defaultTo(999, NaN));
});
