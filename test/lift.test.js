const test = require('ava');
const { Apply, Functor, lift, curry, curryN, map, reduce, ap, head, tail } = require('../src');

test('Should lift a function with one argument', t => {
  const inc = n => n + 1;

  const one = Functor.of(1);

  const liftedInc = lift(inc);

  const result = liftedInc(one);

  t.is(true, result.map !== null);
  t.is('2', result.toString());
});

test('Should lift a function with two arguments', t => {
  const add = (a, b) => a + b;
  const liftedAdd = lift(curry(add));

  const one = Apply.of(1);
  const two = Apply.of(2);

  const result = liftedAdd(one, two);

  t.is('3', result.toString());
});

test('Should lift a function with three arguments', t => {
  const sum = (a, b, c) => a + b + c;
  const liftedSum = lift(curry(sum));

  const one = Apply.of(1);
  const two = Apply.of(2);
  const three = Apply.of(3);

  const result = liftedSum(one, two, three);

  t.is('6', result.toString());
});

test('Should lift a function with an array', t => {
  const inc = a => a + 1;
  const liftedInc = lift(inc);

  const result = liftedInc([1, 2, 3]);

  t.deepEqual([2, 3, 4], result);
});

test('Should lift a function with two arrays', t => {
  const add = curry((a, b) => a + b);
  const liftedAdd = lift(add);

  const args = [[1], [1, 2, 3]];

  const result = liftedAdd([1], [1, 2, 3]);

  t.deepEqual([2, 3, 4], result);
});
