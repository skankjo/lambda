const test = require('ava');
const { isNil } = require('../src');

test('Should be true', t => {
  t.is(true, isNil(null));
  t.is(true, isNil(undefined));
});

test('Should be false', t => {
  t.is(false, isNil(0));
  t.is(false, isNil(-1));
  t.is(false, isNil(''));
  t.is(false, isNil(false));
});
