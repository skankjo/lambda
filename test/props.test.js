const test = require('ava');
const { props } = require('../src');

const obj = {
  name: 'Petras',
  surname: 'Petraitis',
  placeOfBirth: 'Vilnius',
};

test('Should retrieve a list of properties', t => {
  t.deepEqual(['Petras', 'Petraitis'], props(['name', 'surname'], obj));
});

test('Should retrieve an array of values curried', t => {
  t.deepEqual(['Petras', 'Vilnius', 'Petraitis'], props(['name', 'placeOfBirth', 'surname'])(obj));
});
