const test = require('ava');
const { complement, isNil } = require('../src');

test('Should inverse an function', t => {
  const isNotNil = complement(isNil);

  const equals = (a, b) => a === b;
  const notEquals = complement(equals);

  t.is(false, isNotNil(null));
  t.is(true, notEquals(1, 2));
  t.is(false, notEquals(1, 1));
});
