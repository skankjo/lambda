const test = require('ava');
const { sum } = require('../src');

test('Should sum multiple numbers', t => {
  t.is(10, sum([1, 2, 3, 4]));
});
