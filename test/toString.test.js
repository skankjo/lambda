const test = require('ava');
const { toString } = require('../src');

test('Should convert to string', t => {
  t.is('a', toString('a'));
  t.is('1', toString(1));
  t.is('1.1', toString(1.1));
  t.is('1,2', toString([1, 2]));
});
