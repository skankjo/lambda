const test = require('ava');
const { head } = require('../src');

test('Should return a first element of an array', t => {
  t.is('a', head(['a', 'b', 'c']));
});
