const test = require('ava');
const { concat } = require('../src');

test('Should concat arrays', t => {
  t.deepEqual(concat([1, 2], [3, 4]), [1, 2, 3, 4]);
});

test('Should concat strings', t => {
  t.is('ab', concat('a', 'b'));
});

test('Should concat arrays curried', t => {
  t.deepEqual([1, 2, 3, 4], concat([1])([2, 3, 4]));
});

test('Should concat strings curried', t => {
  t.is('ab', concat('a')('b'));
});
