const test = require('ava');
const { Apply, Functor } = require('../../src');

test('Should apply a function', t => {
  const apply = new Apply(a => a + 1);
  const val = new Apply(1);

  const result = apply.ap(val);

  t.is('2', result.toString());
  t.is(true, result instanceof Apply);
});

test('Should return a Functor', t => {
  const apply = new Apply(a => a);
  const val = new Functor(1);

  const result = apply.ap(val);

  t.is('1', result.toString());
  t.is(true, result instanceof Functor);
});

test('Should instantiate an Apply object', t => {
  const obj = Apply.of(a => a + 1);

  t.is(true, obj instanceof Apply);
  t.is('2', obj.ap(Functor.of(1)).toString());
});
