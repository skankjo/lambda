const test = require('ava');
const { Functor } = require('../../src');

test('Should map a Functor to a function', t => {
  const one = new Functor(1);

  const result = one.map(a => a + 1);

  t.is('2', result.toString());
  t.is(true, result instanceof Functor);
});

test('Should create an object of Functor', t => {
  const one = Functor.of(1);

  t.is(true, one instanceof Functor);
  t.is('1', one.toString());
});
