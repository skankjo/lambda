const test = require('ava');
const { sum, unapply } = require('../src');

test('Should make an variadic function out of a function with a parameter as an array', t => {
  const $sum = unapply(sum);
  t.is(10, $sum(1, 2, 3, 4));
});
