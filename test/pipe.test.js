const test = require('ava');
const { pipe, props, sum, unapply, toString } = require('../src');

const obj = {
  a: 1,
  b: 2,
  c: 3,
  d: 4,
};

test('Should pipe several functions', t => {
  t.is(
    10,
    pipe(
      props(['a', 'b', 'c', 'd']),
      sum
    )(obj)
  );
});

test('First function should accept two arguments', t => {
  const add = (a, b) => a + b;

  t.is(
    '10',
    pipe(
      add,
      toString
    )(5, 5)
  );
});

test('First function should accept four arguments', t => {
  const add = unapply(sum);

  t.is(
    '10',
    pipe(
      add,
      toString
    )(1, 2, 3, 4)
  );
});
