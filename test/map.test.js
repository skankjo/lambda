const test = require('ava');
const { Functor, isNil, not, map } = require('../src');

test('Should map functor', t => {
  const result = map(a => a + 1, Functor.of(1));

  t.is('2', result.toString());
});

test('Should map an array', t => {
  const result = map(a => a + 1, [1, 2, 3]);

  t.deepEqual([2, 3, 4], result);
});

test('Should map a value', t => {
  const result = map(a => a, 1);

  t.is(1, result);
});

test('Should take function as a functor', t => {
  const notNil = map(isNil, not);

  t.is(true, notNil('a'));
});

test('Should apply n-arity function', t => {
  const add = (a, b) => a + b;
  const negate = a => -a;
  const addAndNegate = map(add, negate);

  t.is(-2, addAndNegate(1, 1));
});
