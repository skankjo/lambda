const test = require('ava');
const { prop } = require('../src');

const obj = {
  name: 'Jonas',
  surname: 'Jonaitis',
  age: 21,
};

test('Should retrieve a property from an object', t => {
  t.is('Jonas', prop('name', obj));
});

test('Should retrieve a property curried', t => {
  t.is(21, prop('age')(obj));
});
