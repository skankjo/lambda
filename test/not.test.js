const test = require('ava');
const { not } = require('../src');

test('Should inverse an argument', t => {
  t.is(true, not(false));
  t.is(false, not(true));
});
