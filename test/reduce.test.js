const test = require('ava');
const { reduce } = require('../src');

test('Should reduce an array', t => {
  const subtract = (a, b) => a - b;
  t.is(-10, reduce(subtract, 0, [1, 2, 3, 4]));
});
