const test = require('ava');
const { curry } = require('../src');

test('Should curry an add function', t => {
  const add = curry((a, b) => a + b);

  const add2 = add(2);

  t.is(3, add2(1));
  t.is(5, add2(3));
});

test('An array should map a curried function', t => {
  const add = curry((a, b) => a + b);

  const result = [1, 2, 3].map(add(1));

  t.deepEqual([2, 3, 4], result);
});
