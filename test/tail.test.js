const test = require('ava');
const { tail } = require('../src');

test('Should return all but the first element of an array', t => {
  t.deepEqual([2, 3, 4], tail([1, 2, 3, 4]));
});

test('Should return an empty array', t => {
  t.deepEqual([], tail([]));
});
